> Universidad de San Carlos \
Facultad de Ingeniería \
Escuela de Ciencias y Sistemas \
Sofware Avanzado\
Laboratorio

>Pablo Roberto Roche Palacios \
carnet: 2011-22998

---
# Tarea 8

Crear al menos dos contenedores en Docker Compose que involucren:
- Un servidor web
- Una base de datos 
El servidor web debe hacer alguna consulta a la base y presentar información en el puerto 80 del host.
El Docker Compose debe poder darle los datos a la base en algún archivo que la base importe al iniciar y luego sea lo que presente.

---
## Descripción del ejercicio
Se mostrará una página web donde se lleva el control de tareas, que se deben de guardar en una base de datos. Además se pide que la página utilice el puerto 80 del host y que se cargue información desde el archivo Docker-compose

## Contenedores a usar
1. MySQL
Se eligió MySQL como motor de base de datos. Almacenará una tabla que contendrá información de tareas. Cada tarea está compuesta por Título y Descripción.

2. NodeJS / AdonisJS
NodeJS es un entorno de ejecución de JavaScript. AdonisJS es un framework para la administración de páginas web escritas para NodeJS. En este servicio se levantará una página donde se muestra un formulario para ingresar tareas; y las tareas guardadas.

## Docker-compose
![Docker-compose](dc.png)
- Se utiliza la versión 3 de docker-compose.
- Se levantan 2 contenedores: mysql y web
    - mysql: contenedor de la base de datos.
        - Se hace accesible el puerto 3306 del contenedor, por el puerto 3336 del host
        - Se guarda la base de datos en /var/lib/mysql
        - Se agrega la carpeta /dump del repositorio a la carpeta /docker-entrypoint-initdb.d. Acá se encuentra la base de datos que se agregará, usando el comando que abre la consola del contenedor e importando la información.
        - Se agregan variables de entorno, como el nombre de la base, el usuario y la contraseña de inicio
    - web: contenedor de la página web
        - Se construye una imagen a partir de un Dockerfile.
        - Se hace accesible el puerto 80 del contenedor, por el puerto 80 del host.
        - Se obliga a levantar el contendor de la base para poder levantar este contendor.


## Resultado
![Pagina](pantalla.png)

## Video
[![En youtube](https://img.youtube.com/vi/6MP0YpleRj4/0.jpg)](https://youtu.be/6MP0YpleRj4)
