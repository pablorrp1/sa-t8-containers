'use strict'
const Database = use('Database')
const Tarea = use('App/Models/Tarea')

class TareaController {
    async index({view}){
        const tareas = await Tarea.all()
        return view.render('tareas',{
            tareas:tareas.toJSON()
        })
    }

    async store({request, response}){
        const post = new Tarea()
        post.titulo = request.input('titulo')
        post.descripcion = request.input('descripcion')
        await post.save()
        return response.redirect('/')
    }
}

module.exports = TareaController
